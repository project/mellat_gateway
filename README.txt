----------------------
Copyright
----------------------

This module has been published by Meshkin Soft Company http://meshkinsoft.com 
All rights to change or modify the source code of this module is reserved for Meshkin Soft Company.

----------------------
Installation Notes
----------------------

In order to install the module you have to simply install Drupal 8, 
Commerce module and Commerce Payment modules. Then inside payment gateway menu of
the commerce module you can configure the terminal code, username and password of the 
Bank Mellat Terminal in order to attach the gateway to the Mellat Gateway.

